from tkinter import *
import string


def code(text):
    s1=""
    for i in text:
        if ord(i)==ord(' '):
            s1+=' '
        elif ord('а')<=ord(i)<=ord('я'):   
            if ord(i)+5 > ord("я"):
                s1+=chr(ord(i)+5 - 32)
            else:
                s1+=chr(ord(i)+5)
        elif ord('А')<=ord(i)<=ord('Я'):
            if ord(i)+5 > ord("Я"):
                s1+=chr(ord(i)+5 - 32)
            else:
                s1+=chr(ord(i)+5)
        else:
            s1+=i
            

    return s1


class CaesarCodeWindow(Toplevel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.geometry('1920x960')
        f1=Frame(self, width=300)
        f2=Frame(self)

        self.Ent=Entry(f2, width=100)
        self.Str=Label(f2, width=100)

        f3=Frame(self, width=300)

        self.Ent.focus()
        self.Ent.pack(fill=X)
        self.Str.pack(fill=X)
        f1.pack(side=LEFT)
        f2.pack(side=LEFT)
        f3.pack(side=LEFT)
        self.Ent.bind('<KeyRelease>',self.change)
        '''self.mainloop()'''

    def change(self, event, *args, **kwargs ):
        a=self.Ent.get()
        a=code(a)
        self.Str.config(text=a)


