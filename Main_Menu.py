from tkinter import *
from Vigenere_cypher import VigenereWindow, code
from Caesar_code import CaesarCodeWindow, code
from Caesar_encode import CaesarEncodeWindow, encode
from Morze_code import MorseCodeWindow, text_morse
from Morze_encode import MorseEncodeWindow, morse_text, stroka
from litoreya import LitCodeWindow, code 


def create_vigenere():
    '''root.withdraw()'''
    VigenereWindow(root)

def create_caesar_code():
    '''root.withdraw()'''
    CaesarCodeWindow(root)
    
def create_caesar_encode():
    '''root.withdraw()'''
    CaesarEncodeWindow(root)

def create_morse_code():
    '''root.withdraw()'''
    MorseCodeWindow(root)

def create_morse_encode():
    '''root.withdraw()'''
    MorseEncodeWindow(root)

def create_lit_code():
    '''root.withdraw()'''
    LitCodeWindow(root)






root = Tk()

f1=Frame(root, width=30)
f2=Frame(root)
b1 = Button(f2, text="Vigenere", activebackground='red', command=create_vigenere)
b2 = Button(f2, text="Caesar_code", activebackground='red', command=create_caesar_code)
b3 = Button(f2, text="Caesar_encode", activebackground='red', command=create_caesar_encode)
b4 = Button(f2, text="Morse_code", activebackground='red', command=create_morse_code)
b5 = Button(f2, text="Morse_encode", activebackground='red', command=create_morse_encode)
b6 = Button(f2, text="Lit_code", activebackground='red', command=create_lit_code)


f3=Frame(root, width=30)


b1.pack(fill=X)
b2.pack(fill=X)
b3.pack(fill=X)
b4.pack(fill=X)
b5.pack(fill=X)
b6.pack(fill=X)

f1.pack(side=LEFT)
f2.pack(side=LEFT)
f3.pack(side=LEFT)

root.mainloop()
