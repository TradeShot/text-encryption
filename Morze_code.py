from tkinter import *
def text_morse(a):
    
    morse = []
    for i in range(len(a)):
        if a[i] == 'а':
            morse_code = ('• — ')
            morse.append(morse_code)
        elif a[i] == 'б':
            morse_code = ('— • • • ')
            morse.append(morse_code)
        elif a[i] == 'в':
            morse_code = ('• — — ')
          
            morse.append(morse_code)
        elif a[i] == 'г':
            morse_code = ('— — • ' )
      
            morse.append(morse_code)
        elif a[i] == 'д':
            morse_code = ('— • • ')
           
            morse.append(morse_code)
        elif a[i] == 'е':
            morse_code = ('• ')
           
            morse.append(morse_code)
        elif a[i] == 'ё':
            morse_code = ('• ')
            
            morse.append(morse_code)
        elif a[i] == 'ж':
            morse_code = ('• • • — ')
            
            morse.append(morse_code)
        elif a[i] == 'з':
            morse_code = ('— — • • ')
           
            morse.append(morse_code)
        elif a[i] == 'и':
            morse_code = ('• • ')
         
            morse.append(morse_code)
        elif a[i] == 'к':
            morse_code = ('— • — ')
            
            morse.append(morse_code)
        elif a[i] == 'л':
            morse_code = ('• — • • ')
            
            morse.append(morse_code)
        elif a[i] == 'м':
            morse_code = ('— — ')
      
            morse.append(morse_code)
        elif a[i] == 'н':
            morse_code = ('— • ')
     
            morse.append(morse_code)
        elif a[i] == 'о':
            morse_code = ('— — — ')
            
            morse.append(morse_code)
        elif a[i] == 'п':
            morse_code = ('• — — • ')
          
            morse.append(morse_code)
        elif a[i] == 'р':
            morse_code = ('• — • ')
           
            morse.append(morse_code)
        elif a[i] == 'с':
            morse_code = ('• • • ')
            
            morse.append(morse_code)
        elif a[i] == 'т':
            morse_code = ('—')
            
            morse.append(morse_code)
        elif a[i] == 'у':
            morse_code = ('• • — ')
            
            morse.append(morse_code)
        elif a[i] == 'ф':
            morse_code = ('• • — • ')
            
            morse.append(morse_code)
        elif a[i] == 'х':
            morse_code = ('• • • • ')
           
            morse.append(morse_code)
        elif a[i] == 'ц':
            morse_code = ('— • — • ')
           
            morse.append(morse_code)
        elif a[i] == 'ч':
            morse_code = ('— — —  ')
            
            morse.append(morse_code)
        elif a[i] == 'ш':
            morse_code = ('— — — — ')
           
            morse.append(morse_code)
        elif a[i] == 'щ':
            morse_code = ('— — • — ')
           
            morse.append(morse_code)
        elif a[i] == 'ъ':
            morse_code = ('— — • — — ')
            
            morse.append(morse_code)
        elif a[i] == 'ы':
            morse_code = ('— • — — ')
            
            morse.append(morse_code)
        elif a[i] == 'ь':
            morse_code = ('— • • — ')
            morse.append(morse_code)
        elif a[i] == 'ю':
            morse_code = ('• • — — ')
           
            morse.append(morse_code)
        elif a[i] == 'я':
            morse_code = ('• — • — ')
            morse.append(morse_code)
        elif a[i] == ' ':
            morse_code = (' ')
            morse.append(morse_code)
            
    return morse

class MorseCodeWindow(Toplevel):
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.geometry('1920x960')
            f1=Frame(self, width=300)
            f2=Frame(self)

            self.Ent=Entry(f2, width=100)
            self.Str=Label(f2, width=100)

            f3=Frame(self, width=300)

            self.Ent.focus()
            self.Ent.pack(fill=X)
            self.Str.pack(fill=X)
            f1.pack(side=LEFT)
            f2.pack(side=LEFT)
            f3.pack(side=LEFT)
            self.Ent.bind('<KeyRelease>',self.change)

    def change(self, event, *args, **kwargs ):
        a=self.Ent.get()
        a=text_morse(a)
        self.Str.config(text=a)
            
