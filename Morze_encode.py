from tkinter import *


def morse_text(string):
    morse_table=[]
    if string == '.-':
        morse_code = ('а')
        morse_table.append(morse_code)
    elif string == '.......':
        morse_code = (' ')
        morse_table.append(morse_code)
    elif string == '-...':
        morse_code = ('б')
        morse_table.append(morse_code)
    elif string == '.--':
        morse_code = ('в')
        morse_table.append(morse_code)
    elif string == '--.':
        morse_code = ('г')
        morse_table.append(morse_code)
    elif string == '-..':
        morse_code = ('д')
        morse_table.append(morse_code)
    elif string == '.':
        morse_code = ('е')
        morse_table.append(morse_code)
    elif string == '...-':
        morse_code = ('ж')
        morse_table.append(morse_code)
    elif string == '--..•':
        morse_code = ('з')
        morse_table.append(morse_code)
    elif string == '..':
        morse_code = ('и')
        morse_table.append(morse_code)
    elif string == '-.-':
        morse_code = ('к')
        morse_table.append(morse_code)
    elif string == '.-..':
        morse_code = ('л')
        morse_table.append(morse_code)
    elif string == '--':
        morse_code = ('м')
        morse_table.append(morse_code)
    elif string == '-.':
        morse_code = ('н')
        morse_table.append(morse_code)
    elif string == '---':
        morse_code = ('о')
        morse_table.append(morse_code)
    elif string == '.--.':
        morse_code = ('п')
        morse_table.append(morse_code)
    elif string == '.-.':
        morse_code = ('р')
        morse_table.append(morse_code)
    elif string == '...':
        morse_code = ('с')
        morse_table.append(morse_code)
    elif string == '-':
        morse_code = ('т')
        morse_table.append(morse_code)
    elif string == '..-':
        morse_code = ('у')
        morse_table.append(morse_code)
    elif string == '..-.':
        morse_table.append(morse_code)
    elif string == '....':
        morse_code = ('х')
        morse_table.append(morse_code)
    elif string == '-.-.':
        morse_code = ('ц')
        morse_table.append(morse_code)
    elif string == '---':
        morse_code = ('ц')
        morse_table.append(morse_code)
    elif string == '----':
        morse_code = ('ш')
        morse_table.append(morse_code)
    elif string == '--.-':
        morse_code = ('щ')
        morse_table.append(morse_code)
    elif string == '--.--':
        morse_code = ('ъ')
        morse_table.append(morse_code)
    elif string == '-.--':
        morse_code = ('ы')
        morse_table.append(morse_code)
    elif string == '..--':
        morse_code = ('ю')
        morse_table.append(morse_code)
    elif string == '.-.-':
        morse_code = ('я')
        morse_table.append(morse_code)
    return morse_table




def stroka(a):
    text=[]
    morse=a.split()
    for i in morse:
        text.append(morse_text(i))
       
    return text
            
   

class MorseEncodeWindow(Toplevel):
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.geometry('1920x960')
            f1=Frame(self, width=300)
            f2=Frame(self)

            self.Ent=Entry(f2, width=100)
            self.Str=Label(f2, width=100)

            f3=Frame(self, width=300)

            self.Ent.focus()
            self.Ent.pack(fill=X)
            self.Str.pack(fill=X)
            f1.pack(side=LEFT)
            f2.pack(side=LEFT)
            f3.pack(side=LEFT)
            self.Ent.bind('<KeyRelease>',self.change)

    def change(self, event, *args, **kwargs ):
        a=self.Ent.get()
        a=stroka(a)
        self.Str.config(text=a)
