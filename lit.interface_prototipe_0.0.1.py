from tkinter import *
from tkinter import messagebox
import string




def code(str_to_encode):
    coded_string=""
    for i in range(len(str_to_encode)):
        if str_to_encode[i] == "б":
            coded_string += "щ"
        elif str_to_encode[i] == "в":
            coded_string += "ш"
        elif str_to_encode[i] == "г":
            coded_string += "ч"
        elif str_to_encode[i] == "д":
            coded_string += "ц"
        elif str_to_encode[i] == "ж":
            coded_string += "х"
        elif str_to_encode[i] == "з":
            coded_string += "ф"
        elif str_to_encode[i] == "к":
            coded_string += "т"
        elif str_to_encode[i] == "л":
            coded_string += "с"
        elif str_to_encode[i] == "м":
            coded_string += "р"
        elif str_to_encode[i] == "н":
            coded_string += "п"
        elif str_to_encode[i] == "р":
            coded_string += "м"
        elif str_to_encode[i] == "п":
            coded_string += "н"        
        elif str_to_encode[i] == "щ":
            coded_string += "б"
        elif str_to_encode[i] == "ш":
            coded_string += "в"
        elif str_to_encode[i] == "ч":
            coded_string += "г"
        elif str_to_encode[i] == "ц":
            coded_string += "д"
        elif str_to_encode[i] == "х":
            coded_string += "ж"
        elif str_to_encode[i] == "ф":
            coded_string += "з"
        elif str_to_encode[i] == "т":
            coded_string += "к"
        elif str_to_encode[i] == "с":
            coded_string += "л"
        else:
            coded_string += str_to_encode[i]        
        
    return coded_string


      
    


def show_message():
    messagebox.showinfo("GUI Python", code(message.get()))

root = Tk()


root.title("CAESAR")
root.geometry("300x250")



message = StringVar()

message_label = Label(text="Введите текст:")
message_label.grid(row=0, column=0, sticky="w")
 
message_entry = Entry(textvariable=message)
message_entry.grid(row=0,column=1, padx=5, pady=5 )
 
message_button = Button(text="Click Me", command=show_message)
message_button.place(relx=.5, rely=.5, anchor="c")


root.mainloop()

