from tkinter import *
from tkinter import messagebox
import string




def code(str_to_encode):
    coded_string=""
    for i in range(len(str_to_encode)):
        if str_to_encode[i] == "б":
            coded_string += "щ"
        elif str_to_encode[i] == "в":
            coded_string += "ш"
        elif str_to_encode[i] == "г":
            coded_string += "ч"
        elif str_to_encode[i] == "д":
            coded_string += "ц"
        elif str_to_encode[i] == "ж":
            coded_string += "х"
        elif str_to_encode[i] == "з":
            coded_string += "ф"
        elif str_to_encode[i] == "к":
            coded_string += "т"
        elif str_to_encode[i] == "л":
            coded_string += "с"
        elif str_to_encode[i] == "м":
            coded_string += "р"
        elif str_to_encode[i] == "н":
            coded_string += "п"
        elif str_to_encode[i] == "р":
            coded_string += "м"
        elif str_to_encode[i] == "п":
            coded_string += "н"        
        elif str_to_encode[i] == "щ":
            coded_string += "б"
        elif str_to_encode[i] == "ш":
            coded_string += "в"
        elif str_to_encode[i] == "ч":
            coded_string += "г"
        elif str_to_encode[i] == "ц":
            coded_string += "д"
        elif str_to_encode[i] == "х":
            coded_string += "ж"
        elif str_to_encode[i] == "ф":
            coded_string += "з"
        elif str_to_encode[i] == "т":
            coded_string += "к"
        elif str_to_encode[i] == "с":
            coded_string += "л"
        else:
            coded_string += str_to_encode[i]        
        
    return coded_string



class LitCodeWindow(Toplevel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.geometry('1920x960')
        f1=Frame(self, width=300)
        f2=Frame(self)

        self.Ent=Entry(f2, width=100)
        self.Str=Label(f2, width=100)

        f3=Frame(self, width=300)

        self.Ent.focus()
        self.Ent.pack(fill=X)
        self.Str.pack(fill=X)
        f1.pack(side=LEFT)
        f2.pack(side=LEFT)
        f3.pack(side=LEFT)
        self.Ent.bind('<KeyRelease>',self.change)
        '''self.mainloop()'''

    def change(self, event, *args, **kwargs ):
        a=self.Ent.get()
        a=code(a)
        self.Str.config(text=a)


